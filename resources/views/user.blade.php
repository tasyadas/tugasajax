<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <div>
            <input type="text" name="name">
            <ul>
                <li v-for="(user, index) in users">
                    <span>@{{ user.name }}</span>
                    <button>Edit</button>
                    <button v-on:click="removeUser(index, user)">Delete</button>
                </li>
            </ul>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script>
        new Vue({
            el: '#app',
            data: {
                users: []
            },
            methods: {
                updateUser: function () {
                    // POST /someUrl
                    // this.$http.post('/api/index', {foo: 'bar'}).then(response => {
                    //
                    //     // get status
                    //     response.status;
                    //
                    //     // get status text
                    //     response.statusText;
                    //
                    //     // get 'Expires' header
                    //     response.headers.get('Expires');
                    //
                    //     // get body data
                    //     this.someData = response.body;
                    // });
                },
                removeUser: function (index, user) {
                    this.$http.post(`/api/delete/${user.id}`).then(response => {
                        this.users.splice(index, 1);
                    });
                }
            },
            mounted: function () {
                this.$http.get('/api/index').then(response => {
                    this.users = response.body;
                })
            }
        })
    </script>
</body>
</html>
